# Geo-Temporally Weighted Regression in Apache Spark

Implementation of geo-temporally weighted regression using Apache Spark, Spark ML and Apache Sedona.