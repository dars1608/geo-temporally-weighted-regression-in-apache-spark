package hr.fer.tel.spark.ml.gtwr.cluster

import hr.fer.tel.spark.ml.gtwr.cluster
import org.apache.spark.ml.clustering.{GaussianMixture, GaussianMixtureModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * Geo-temporal clustering algorithm based on classic K-means clustering algorithm.
 *
 * @param templateModel instance of [[GaussianMixture]] object with pre-set model parameters
 * @param _gtcs instance of [[GeoTemporalCoordinateSystemTransformer]]
 * @param spark [[SparkSession]] object
 */
class GeoTemporalGaussianMixture(val templateModel: GaussianMixture,
                        _gtcs: GeoTemporalCoordinateSystemTransformer)(implicit spark: SparkSession)
  extends BaseGeoTemporalClustering(_gtcs) {

  override protected def fitInternal(df: DataFrame, geoTemporalCoordsCol: String, clusterCol: String): GeoTemporalClusteringModel = {
    val tempModel = templateModel.copy(ParamMap.empty)
    tempModel.setFeaturesCol(geoTemporalCoordsCol)
    tempModel.setPredictionCol(clusterCol)

    new GeoTemporalGaussianMixtureModel(tempModel.fit(df), _gtcs)
  }
}

object GeoTemporalGaussianMixtureModel extends GeoTemporalClusteringModelLoader {

  override def load(path: String)(implicit spark: SparkSession): GeoTemporalClusteringModel = {
    new cluster.GeoTemporalGaussianMixtureModel(GaussianMixtureModel.load(path), GeoTemporalCoordinateSystemTransformer.load(path))
  }
}

/**
 * This class represents fitted geo-temporal K-means model.
 *
 * @param _model underlying K-means model
 * @param _gtcs instance of [[GeoTemporalCoordinateSystemTransformer]]
 * @param spark [[SparkSession]] object
 */
class GeoTemporalGaussianMixtureModel(private val _model: GaussianMixtureModel,
                             private val _gtcs: GeoTemporalCoordinateSystemTransformer)(implicit spark: SparkSession)
  extends BaseGeoTemporalClusteringModel(_gtcs) {

  override protected def transformInternal(df: DataFrame): DataFrame = _model.transform(df)

  override def k: Int = _model.getK

  /**
   * @return underlying K-means model
   */
  def underlyingModel: GaussianMixtureModel = _model

  override def save(path: String): Unit = {
    _model.save(path)
    _gtcs.save(path)
  }
}
