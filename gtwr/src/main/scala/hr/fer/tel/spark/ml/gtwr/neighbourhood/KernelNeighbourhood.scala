package hr.fer.tel.spark.ml.gtwr.neighbourhood

import org.apache.spark.sql.DataFrame

/**
 * This trait defines an object responsible for finding the neighbourhood for each sample in input dataframe
 * and weighting them using the kernel function applied to the geo-temporal distance.
 */
trait KernelNeighbourhood extends Serializable {

  /**
   * This method is used for the neighbourhood for each sample in input dataframe
   * and weighting them using the kernel function applied to the geo-temporal distance.
   *
   * @param df input dataframe
   * @param geometryCol name of geometry column (of type [[com.vividsolutions.jts.geom.Geometry]])
   * @param timestampCol name of timestamp column (of type [[org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp]])
   * @param featuresCol name of features column (of type [[breeze.linalg.Vector]])
   * @param labelCol name of label column
   * @param weightCol name of weight column (output column)
   * @param rowNumberCol name of row number column (output column)
   * @param kernelName name of kernel function
   * @param trainView name of train view (input table)
   * @param unknownView name of unknown view (output table)
   * @return transformed dataframe
   */
  def find(df: DataFrame,
           geometryCol: String = "geometry",
           timestampCol: String = "ts",
           featuresCol: String = "features",
           labelCol: String = "label",
           weightCol: String = "weight",
           rowNumberCol: String = "rn",
           kernelName: String = "kernel",
           trainView: String = "traindf",
           unknownView: String = "unknowndf"): DataFrame
}
