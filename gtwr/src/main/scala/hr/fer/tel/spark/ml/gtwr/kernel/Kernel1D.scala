package hr.fer.tel.spark.ml.gtwr.kernel

/**
 * One-dimensional kernel function used for calculating weights for each sample in regression set.
 */
trait Kernel1D extends Serializable {

  /**
   * This function applies kernel function to given value.
   * @param value value to transform
   * @param bandwidth kernel bandwidth
   * @return value transformed using kernel with given bandwidth
   */
  def transform(value: Double, bandwidth: Double): Double
}

/**
 * Kernel defined with bi-square kernel function.
 */
class BiSquareKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double =
    if (math.abs(value) < bandwidth) math.pow(1 - math.pow(value / bandwidth, 2), 2) else 0
}

/**
 * Kernel defined with box car kernel function.
 */
class BoxCarKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double =
    if (math.abs(value) < bandwidth) 1 else 0
}

/**
 * Kernel defined with exponential kernel function.
 */
class ExponentialKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double =
    if (bandwidth == 0) 1 else math.exp(-0.5 * math.abs(value / bandwidth))
}

/**
 * Kernel defined with Gaussian kernel function.
 */
class GaussianKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double =
    if (bandwidth == 0) 1 else math.exp(-0.5 * Math.pow(value / bandwidth, 2))
}

/**
 * Kernel defined with tri-cube kernel function.
 */
class TriCubeKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double =
    if (math.abs(value) < bandwidth) math.pow(1 - math.pow(value / bandwidth, 3), 3) else 0
}

/**
 * Kernel defined with uniform kernel function.
 */
class UniformKernel extends Kernel1D {

  override def transform(value: Double, bandwidth: Double): Double = 1
}

