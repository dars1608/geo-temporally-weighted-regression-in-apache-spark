package hr.fer.tel.spark.ml.gtwr.cluster

import hr.fer.tel.spark.ml.gtwr.cluster
import org.apache.spark.ml.clustering.{KMeans, KMeansModel}
import org.apache.spark.ml.param.ParamMap
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * Geo-temporal clustering algorithm based on classic K-means clustering algorithm.
 *
 * @param templateModel instance of [[KMeans]] object with pre-set model parameters
 * @param _gtcs instance of [[GeoTemporalCoordinateSystemTransformer]]
 * @param spark [[SparkSession]] object
 */
class GeoTemporalKMeans(val templateModel: KMeans,
                        _gtcs: GeoTemporalCoordinateSystemTransformer)(implicit spark: SparkSession)
  extends BaseGeoTemporalClustering(_gtcs) {

  override protected def fitInternal(df: DataFrame, geoTemporalCoordsCol: String, clusterCol: String): GeoTemporalClusteringModel = {
    val tempModel = templateModel.copy(ParamMap.empty)
    tempModel.setFeaturesCol(geoTemporalCoordsCol)
    tempModel.setPredictionCol(clusterCol)

    new GeoTemporalKMeansModel(tempModel.fit(df), _gtcs)
  }
}

object GeoTemporalKMeansModel extends GeoTemporalClusteringModelLoader {

  override def load(path: String)(implicit spark: SparkSession): GeoTemporalClusteringModel = {
    new cluster.GeoTemporalKMeansModel(KMeansModel.load(path), GeoTemporalCoordinateSystemTransformer.load(path))
  }
}

/**
 * This class represents fitted geo-temporal K-means model.
 *
 * @param _model underlying K-means model
 * @param _gtcs instance of [[GeoTemporalCoordinateSystemTransformer]]
 * @param spark [[SparkSession]] object
 */
class GeoTemporalKMeansModel(private val _model: KMeansModel,
                             private val _gtcs: GeoTemporalCoordinateSystemTransformer)(implicit spark: SparkSession)
  extends BaseGeoTemporalClusteringModel(_gtcs) {

  override protected def transformInternal(df: DataFrame): DataFrame = _model.transform(df)

  override def k: Int = _model.getK

  /**
   * @return underlying K-means model
   */
  def underlyingModel: KMeansModel = _model

  override def save(path: String): Unit = {
    _model.save(path)
    _gtcs.save(path)
  }
}
