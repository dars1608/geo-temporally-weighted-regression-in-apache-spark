package hr.fer.tel.spark.ml.gtwr

import hr.fer.tel.spark.ml.gtwr.kernel.{Kernel2D, Uniform2DKernel}
import hr.fer.tel.spark.ml.gtwr.neighbourhood.{FixedKernelNeighbourhood, KernelNeighbourhood}
import org.apache.spark.ml.param._
import org.apache.spark.ml.regression.GeneralizedLinearRegression
import org.apache.spark.ml.util.Identifiable
import org.apache.spark.ml.{Estimator, Model}
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types.{DoubleType, StructField, StructType}
import org.apache.spark.sql.{DataFrame, Dataset, Row, SparkSession}

import java.util.Objects.nonNull
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

trait HasGeometryCol extends Params {
  def geometryCol: Param[String] = new Param[String](this, "geometryCol", "Geometry column.")

  setDefault(ParamPair(geometryCol, "geometry"))

  def getGeometryCol: String = $(geometryCol)
}

trait HasTimestampCol extends Params {
  def timestampCol: Param[String] = new Param[String](this, "timestampCol", "Timestamp column.")

  setDefault(ParamPair(timestampCol, "ts"))

  def getTimestampCol: String = $(timestampCol)
}

trait HasGeneralizedLinearRegression extends Params {
  def generalizedLinearRegression: Param[GeneralizedLinearRegression] = new Param[GeneralizedLinearRegression](
    this, "generalisedLinearRegression", "GeneralizedLinearRegression used as underlying Regressor implementation."
  )

  setDefault(generalizedLinearRegression, new GeneralizedLinearRegression())

  def getGeneralizedLinearRegression: GeneralizedLinearRegression = $(generalizedLinearRegression)
}

trait GeoTemporallyWeightedRegressionParams extends Params
  with HasGeometryCol with HasTimestampCol with HasGeneralizedLinearRegression {

  def kernelNeighbourhood: Param[KernelNeighbourhood] = new Param[KernelNeighbourhood](
    this, "kernelNeighbourhood", "KernelNeighbourhood implementation used for finding train sets for fitting local Regressors."
  )

  setDefault(kernelNeighbourhood, new FixedKernelNeighbourhood(spatialBandwidthMeters = Double.MaxValue))

  def getKernelNeighbourhood: KernelNeighbourhood = $(kernelNeighbourhood)

  def kernel: Param[Kernel2D] = new Param[Kernel2D](
    this, "kernel", "Kernel2D implementation used for calculating weights of each record in local training set."
  )

  setDefault(kernel, new Uniform2DKernel())

  def getKernel: Kernel2D = $(kernel)

  def maxInputSize: IntParam = new IntParam(this, "maxInputSize", "Maximum input size.", { (x: Int) => x > 0 })

  setDefault(maxInputSize, 1000)

  def getMaxInputSize: Int = $(maxInputSize)

  def maxJobDuration: Param[Duration] = new Param[Duration](this, "maxJobDuration", "Maximum job duration")

  setDefault(maxJobDuration, Duration.Inf)

  def getMaxJobDuration: Duration = $(maxJobDuration)
}

object GeoTemporallyWeightedRegression {
  private[gtwr] val trainView = "traindf"
  private[gtwr] val unknownView = "unknowndf"
  private[gtwr] val weightCol = "weight"
  private[gtwr] val kernelFunc = "kernel"
  private[gtwr] val rowNumberCol = "rn"
}

// NOTE: This implementation isn't scalable because it requires too much tasks and
// cannot transform (predict) on datasets which don't fit in the memory of the master node
class GeoTemporallyWeightedRegression(override val uid: String)(implicit spark: SparkSession)
  extends Estimator[GeoTemporallyWeightedRegressionModel]
    with Serializable with GeoTemporallyWeightedRegressionParams {

  import GeoTemporallyWeightedRegression._

  def this()(implicit spark: SparkSession) = this(Identifiable.randomUID("gtwr"))(spark)

  def setGeometryCol(value: String): GeoTemporallyWeightedRegression = set(geometryCol, value)

  def setTimestampCol(value: String): GeoTemporallyWeightedRegression = set(timestampCol, value)

  def setKernelNeighbourhood(value: KernelNeighbourhood): GeoTemporallyWeightedRegression = set(kernelNeighbourhood, value)

  def setKernel(value: Kernel2D): GeoTemporallyWeightedRegression = set(kernel, value)

  def setMaxInputSize(value: Int): GeoTemporallyWeightedRegression = set(maxInputSize, value)

  def setMaxJobDuration(value: Duration): GeoTemporallyWeightedRegression = set(maxJobDuration, value)

  def setGeneralizedLinearRegression(value: GeneralizedLinearRegression): GeoTemporallyWeightedRegression = {
    value.setWeightCol(weightCol)
    set(generalizedLinearRegression, value)
  }

  override def fit(dataset: Dataset[_]): GeoTemporallyWeightedRegressionModel = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    new GeoTemporallyWeightedRegressionModel(dataset.toDF())
      .setGeometryCol(getGeometryCol)
      .setTimestampCol(getTimestampCol)
      .setKernelNeighbourhood(getKernelNeighbourhood)
      .setKernel(getKernel)
      .setMaxInputSize(getMaxInputSize)
      .setMaxJobDuration(getMaxJobDuration)
      .setGeneralizedLinearRegression(getGeneralizedLinearRegression)
  }

  override def copy(extra: ParamMap): Estimator[GeoTemporallyWeightedRegressionModel] = {
    new GeoTemporallyWeightedRegression(uid)
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setTimestampCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setKernelNeighbourhood(extra.getOrElse(kernelNeighbourhood, getKernelNeighbourhood))
      .setKernel(extra.getOrElse(kernel, getKernel))
      .setMaxInputSize(extra.getOrElse(maxInputSize, getMaxInputSize))
      .setMaxJobDuration(extra.getOrElse(maxJobDuration, getMaxJobDuration))
      .setGeneralizedLinearRegression(extra.getOrElse(generalizedLinearRegression, getGeneralizedLinearRegression))
  }

  override def transformSchema(schema: StructType): StructType = schema
}

class GeoTemporallyWeightedRegressionModel private[gtwr](override val uid: String,
                                                         private val _trainDf: DataFrame)(implicit spark: SparkSession)
  extends Model[GeoTemporallyWeightedRegressionModel] with GeoTemporallyWeightedRegressionParams with Serializable {

  import GeoTemporallyWeightedRegression._

  private[gtwr] def this(trainDf: DataFrame)(implicit spark: SparkSession) =
    this(Identifiable.randomUID("gtwrModel"), trainDf)(spark)

  def setGeometryCol(value: String): GeoTemporallyWeightedRegressionModel = set(geometryCol, value)

  def setTimestampCol(value: String): GeoTemporallyWeightedRegressionModel = set(timestampCol, value)

  def setKernelNeighbourhood(value: KernelNeighbourhood): GeoTemporallyWeightedRegressionModel = set(kernelNeighbourhood, value)

  def setKernel(value: Kernel2D): GeoTemporallyWeightedRegressionModel = set(kernel, value)

  def setMaxInputSize(value: Int): GeoTemporallyWeightedRegressionModel = set(maxInputSize, value)

  def setMaxJobDuration(value: Duration): GeoTemporallyWeightedRegressionModel = set(maxJobDuration, value)

  def setGeneralizedLinearRegression(value: GeneralizedLinearRegression): GeoTemporallyWeightedRegressionModel = {
    value.setWeightCol(weightCol)
    set(generalizedLinearRegression, value)
  }


  def transform(dataset: Dataset[_]): DataFrame = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    val df = dataset.toDF()

    import org.apache.spark.sql.functions.{lit, row_number}
    import spark.implicits._

    _trainDf.createOrReplaceTempView(trainView)

    val w = Window.orderBy(lit("A"))
    var tempDf = df.withColumn(rowNumberCol, row_number().over(w))

    tempDf.createOrReplaceTempView(unknownView)
    spark.udf.register(kernelFunc,
      (v1: Double, b1: Double, v2: Double, b2: Double) => getKernel.transform(v1, b1, v2, b2)
    )

    tempDf = getKernelNeighbourhood.find(
      tempDf,
      geometryCol = getGeometryCol,
      timestampCol = getTimestampCol,
      featuresCol = getGeneralizedLinearRegression.getFeaturesCol,
      labelCol = getGeneralizedLinearRegression.getLabelCol,
      weightCol = weightCol,
      rowNumberCol = rowNumberCol,
      kernelName = kernelFunc,
      trainView = trainView,
      unknownView = unknownView
    )

    val inputCount = df.count()

    if (inputCount > getMaxInputSize.toLong) {
      throw new IllegalArgumentException(s"Input is too big.")
    }

    tempDf = tempDf.repartitionByRange(inputCount.toInt, $"${unknownView}_$rowNumberCol")
    tempDf.persist()

    val hasLabelCol = df.columns.contains(s"${getGeneralizedLinearRegression.getLabelCol}")
    val predictRows = (1 to inputCount.toInt) map { rn =>
      Future {
        val glrLocal = getGeneralizedLinearRegression.copy(ParamMap.empty)

        val sampleDf = tempDf.filter($"${unknownView}_$rowNumberCol" === rn)
        val trainDf = sampleDf.selectExpr(
          s"${trainView}_${getGeneralizedLinearRegression.getFeaturesCol} AS ${getGeneralizedLinearRegression.getFeaturesCol}",
          s"${trainView}_${getGeneralizedLinearRegression.getLabelCol} AS ${getGeneralizedLinearRegression.getLabelCol}",
          "weight"
        )

        if(trainDf.count() == 0){
          null
        } else {
          val testSample = if (hasLabelCol) {
            sampleDf.selectExpr(
              s"${unknownView}_${getGeneralizedLinearRegression.getFeaturesCol} AS ${getGeneralizedLinearRegression.getFeaturesCol}",
              s"${unknownView}_${getGeneralizedLinearRegression.getLabelCol} AS ${getGeneralizedLinearRegression.getLabelCol}",
              s"${unknownView}_$getGeometryCol AS $getGeometryCol",
              s"${unknownView}_$getTimestampCol AS $getTimestampCol"
            ).first()
          } else {
            sampleDf.selectExpr(
              s"${unknownView}_${getGeneralizedLinearRegression.getFeaturesCol} AS ${getGeneralizedLinearRegression.getFeaturesCol}",
              s"${unknownView}_$getGeometryCol AS $getGeometryCol",
              s"${unknownView}_$getTimestampCol AS $getTimestampCol"
            ).first()
          }
          val testDf = spark.createDataFrame(spark.sparkContext.parallelize(Seq(testSample)), testSample.schema)

          val predictDf = glrLocal.fit(trainDf)
            .transform(testDf)

          predictDf.first()
        }
      }(ExecutionContext.global)
    } map {
      job => Await.ready(job, getMaxJobDuration)
    } map {
      job => Await.result(job, getMaxJobDuration)
    } filter nonNull toList

    tempDf.unpersist()
    spark.createDataFrame(spark.sparkContext.makeRDD[Row](predictRows), predictRows.head.schema)
  }

  override def copy(extra: ParamMap): GeoTemporallyWeightedRegressionModel = {
    new GeoTemporallyWeightedRegressionModel(uid, _trainDf)
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setKernelNeighbourhood(extra.getOrElse(kernelNeighbourhood, getKernelNeighbourhood))
      .setKernel(extra.getOrElse(kernel, getKernel))
      .setMaxInputSize(extra.getOrElse(maxInputSize, getMaxInputSize))
      .setMaxJobDuration(extra.getOrElse(maxJobDuration, getMaxJobDuration))
      .setGeneralizedLinearRegression(extra.getOrElse(generalizedLinearRegression, getGeneralizedLinearRegression))
  }

  override def transformSchema(schema: StructType): StructType =
    schema.add(StructField(getGeneralizedLinearRegression.getPredictionCol, DoubleType))
}
