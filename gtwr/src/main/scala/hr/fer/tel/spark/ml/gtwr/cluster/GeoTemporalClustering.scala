package hr.fer.tel.spark.ml.gtwr.cluster

import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * This trait defines a generic clustering algorithm which clusters data based geo-temporal data.
 */
trait GeoTemporalClustering {

  /**
   * This method is used for fitting the clustering model using the supplied data.
   * @param df input dataframe
   * @param prepared true if dataframe has precalculated geo-temporal coordinates column
   * @param geometryCol name of geometry column (of type [[com.vividsolutions.jts.geom.Geometry]])
   * @param timestampCol name of timestamp column (of type [[org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp]])
   * @param geoTemporalCoordsCol name of geo-temporal coords column (of type [[breeze.linalg.Vector]])
   * @param clusterCol name of output column which will include cluster id's
   * @return instance of fitted [[GeoTemporalClusteringModel]]
   */
  def fit(df: DataFrame,
          prepared: Boolean = false,
          geometryCol: String = "geometry",
          timestampCol: String = "ts",
          geoTemporalCoordsCol: String = "gtc",
          clusterCol: String = "cluster"): GeoTemporalClusteringModel
}

/**
 * This trait defines a helper object used for loading the [[GeoTemporalClusteringModel]] saved on disk.
 */
trait GeoTemporalClusteringModelLoader {

  /**
   * This method loads an instance of [[GeoTemporalClusteringModel]] from the disk into the memory.
   * @param path path to the saved model
   * @param spark [[SparkSession]] object
   * @return an instance of [[GeoTemporalClusteringModel]]
   */
  def load(path: String)(implicit spark: SparkSession): GeoTemporalClusteringModel
}

/**
 * This trait defines fitted generic clustering algorithm which clusters data based geo-temporal data.
 */
trait GeoTemporalClusteringModel {

  /**
   * This method is used for clustering input data based on fitted clustering model.
   * @param df input dataframe
   * @param prepared true if dataframe has precalculated geo-temporal coordinates column
   * @param geometryCol name of geometry column (of type [[com.vividsolutions.jts.geom.Geometry]])
   * @param timestampCol name of timestamp column (of type [[org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp]])
   * @param geoTemporalCoordsCol name of geo-temporal coords column (of type [[breeze.linalg.Vector]])
   * @return
   */
  def transform(df: DataFrame,
                         prepared: Boolean = false,
                         geometryCol: String = "geometry",
                         timestampCol: String = "ts",
                         geoTemporalCoordsCol: String = "gtc"): DataFrame

  /**
   * @return number of clusters
   */
  def k: Int

  /**
   * Method used for saving the model on the disk.
   * @param path location of saved model
   */
  def save(path: String): Unit
}
