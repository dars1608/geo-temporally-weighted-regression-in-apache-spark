package hr.fer.tel.spark.ml.gtwr.cluster

import com.vividsolutions.jts.geom.Geometry
import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.PrintWriter
import scala.io.Source


object GeoTemporalCoordinateSystemTransformer {

  /**
   * This method is used for loading the transformer from the disk.
   *
   * @param path save location of the transformer parameters
   * @return instance of [[GeoTemporalCoordinateSystemTransformer]]
   */
  def load(path: String): GeoTemporalCoordinateSystemTransformer = {
    try {
      val columnsSource = Source.fromFile(s"$path/transformer-params")
      val columns = columnsSource.mkString.split(",")

      new GeoTemporalCoordinateSystemTransformer(columns(0).toDouble, columns(1).toDouble)
    } catch {
      case ex: Exception =>
        throw new IllegalArgumentException(
          s"Loading instance of ${GeoTemporalCoordinateSystemTransformer.getClass} from $path/transformer-params failed",
          ex
        )
    }
  }
}

/**
 * Utility class used for transforming geo-temporal coordinates to accommodate the modified spatio-temporal distance function.
 *
 * ''
 * d = sqrt(lambda * ((x1 - x0)**2 + (y1 - y0)**2) + mi * (t1 - t0)**2)
 * ''
 *
 * @param lambda weight of spatial distance
 * @param mi weight of temporal distance
 */
class GeoTemporalCoordinateSystemTransformer(val lambda: Double = 1.0,
                                             val mi: Double = 1.0) {

  private val centroidXCol = "centroid_x"
  private val centroidYCol = "centroid_y"
  private val timestampSecondsCol = "timestamp_seconds"

  private val stXUdf = "ST_X"
  private val stYUdf = "ST_Y"

  private val sqrtLambda = math.sqrt(lambda)
  private val sqrtMi = math.sqrt(mi)

  /**
   * This method is used for creating column of type [[breeze.linalg.Vector]] which represents geo-temporal coordinates
   * transformed from original geospatial and temporal data.
   *
   * @param df input dataframe
   * @param geometryCol name of geometry column (of type [[com.vividsolutions.jts.geom.Geometry]])
   * @param timestampCol name of timestamp column (of type [[org.apache.spark.sql.execution.streaming.FileStreamSource.Timestamp]])
   * @param outputCol name of geo-temporal coords column (of type [[breeze.linalg.Vector]])
   * @param spark [[SparkSession]] object
   * @return output dataframe
   */
  def transform(df: DataFrame,
                geometryCol: String,
                timestampCol: String,
                outputCol: String)(implicit spark: SparkSession): DataFrame = {
    import org.apache.spark.sql.functions.expr

    spark.udf.register(stXUdf, (geometry: Geometry) => geometry.getCentroid.getCoordinate.x)
    spark.udf.register(stYUdf, (geometry: Geometry) => geometry.getCentroid.getCoordinate.y)

    val tempDf = df
      .withColumn(centroidXCol, expr(f"$stXUdf($geometryCol) * $sqrtLambda"))
      .withColumn(centroidYCol, expr(f"$stYUdf($geometryCol) * $sqrtLambda"))
      .withColumn(timestampSecondsCol, expr(f"unix_timestamp($timestampCol) * $sqrtMi"))

    new VectorAssembler()
      .setInputCols(Array(centroidXCol, centroidYCol, timestampSecondsCol))
      .setOutputCol(outputCol)
      .transform(tempDf)
  }

  /***
   * This method is used for saving the transformer definition to disk which can then be loaded using
   * the [[GeoTemporalCoordinateSystemTransformer.load]] method.
   * @param path save location for the transformer parameters
   */
  def save(path: String): Unit = {
    new PrintWriter(s"$path/transformer-params") {
      write(s"$lambda,$mi")
      close()
    }
  }
}
