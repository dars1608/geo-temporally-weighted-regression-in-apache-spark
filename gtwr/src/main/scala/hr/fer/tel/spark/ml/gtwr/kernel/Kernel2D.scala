package hr.fer.tel.spark.ml.gtwr.kernel

/**
 * Two-dimensional kernel function used for calculating weights for each sample in regression set.
 */
trait Kernel2D extends Serializable {

  /**
   * This function applies kernel function to given values.
   * @param value1 first value
   * @param bandwidth1 first bandwidth
   * @param value2 second value
   * @param bandwidth2 second bandwidth
   * @return
   */
  def transform(value1: Double,
                bandwidth1: Double,
                value2: Double,
                bandwidth2: Double): Double
}

/**
 * Template class for two-dimensional kernel defined as a multiplication of two one-dimensional kernels.
 * @param kernel1 first kernel
 * @param kernel2 second kernel
 */
class KernelMultiplicationWeightingScheme(val kernel1: Kernel1D,
                                          val kernel2: Kernel1D) extends Kernel2D {

  override def transform(value1: Double,
                         bandwidth1: Double,
                         value2: Double,
                         bandwidth2: Double): Double =
    kernel1.transform(value1, bandwidth1) * kernel2.transform(value2, bandwidth2)
}

/**
 * Uniform two-dimensional kernel.
 */
class Uniform2DKernel extends KernelMultiplicationWeightingScheme(new UniformKernel, new UniformKernel)

/**
 * Gaussian two-dimensional kernel.
 */
class Gaussian2DKernel extends KernelMultiplicationWeightingScheme(new GaussianKernel, new GaussianKernel)

/**
 * Bi-square two-dimensional kernel.
 */
class BiSquare2DKernel extends KernelMultiplicationWeightingScheme(new BiSquareKernel, new BiSquareKernel)
