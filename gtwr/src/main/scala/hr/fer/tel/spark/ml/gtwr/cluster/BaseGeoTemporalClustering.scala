package hr.fer.tel.spark.ml.gtwr.cluster

import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * Template class for the [[GeoTemporalClustering]] implementation which does the data preparation for the clustering algorithm.
 * @param gtcs instance of [[GeoTemporalCoordinateSystemTransformer]] which is used for constructing and transforming
 *             geo-temporal coordinate system
 * @param spark [[SparkSession]] object
 */
abstract class BaseGeoTemporalClustering(private val gtcs : GeoTemporalCoordinateSystemTransformer) (implicit spark: SparkSession)
  extends GeoTemporalClustering with Serializable {

  /**
   * This method is used for fit the internal clustering model.
   * @param df input dataframe
   * @param geoTemporalCoordsCol name of geo-temporal coords column (of type [[breeze.linalg.Vector]])
   * @param clusterCol name of output column which will include cluster id's
   * @return instance of fitted [[GeoTemporalClusteringModel]]
   */
  protected def fitInternal(df: DataFrame, geoTemporalCoordsCol: String, clusterCol: String): GeoTemporalClusteringModel

  override def fit(df: DataFrame,
                   prepared: Boolean = false,
                   geometryCol: String = "geometry",
                   timestampCol: String = "ts",
                   geoTemporalCoordsCol: String = "gtc",
                   clusterCol: String = "cluster"): GeoTemporalClusteringModel = {
    fitInternal(
      if (prepared) df else gtcs.transform(df, geometryCol, timestampCol, geoTemporalCoordsCol),
      geoTemporalCoordsCol,
      clusterCol
    )
  }
}

/**
 * Template class for the [[GeoTemporalClusteringModel]] implementation which does the data preparation for the clustering algorithm.
 * @param gtcs instance of [[GeoTemporalCoordinateSystemTransformer]] which is used for constructing and transforming
 *             geo-temporal coordinate system
 * @param spark [[SparkSession]] object
 */
abstract class BaseGeoTemporalClusteringModel(val gtcs : GeoTemporalCoordinateSystemTransformer) (implicit spark: SparkSession)
  extends GeoTemporalClusteringModel with Serializable {

  /**
   * This method is used for clustering the input data using the internal clustering model.
   * @param df input dataframe
   * @return transformed dataframe
   */
  protected def transformInternal(df: DataFrame): DataFrame

  override def transform(df: DataFrame,
                         prepared: Boolean = false,
                         geometryCol: String = "geometry",
                         timestampCol: String = "ts",
                         geoTemporalCoordsCol: String = "gtc"): DataFrame = {
    transformInternal(
      if (prepared) df else gtcs.transform(df, geometryCol, timestampCol, geoTemporalCoordsCol)
    )
  }
}