package hr.fer.tel.spark.ml.gtwr.neighbourhood

import org.apache.spark.sql.DataFrame

/**
 * Kernel neighbourhood which calculates spatial and temporal bandwidth based on a predefined number of neighbours
 * for each local regression model. Nearest neighbours are calculated using the weighted spatio-temporal distance.
 *
 * ''
 * d = sqrt(lambda * ((x1 - x0)**2 + (y1 - y0)**2) + mi * (t1 - t0)**2)
 * ''
 *
 * @param bandwidthNeighbours number of neighbours for each local model
 * @param lambda weight of spatial distance
 * @param mi weight of temporal distance
 */
class AdaptiveKernelNeighbourhood(val bandwidthNeighbours: Int = 10,
                                  val lambda: Double = 1.0,
                                  val mi: Double = 1.0) extends KernelNeighbourhood {

  val tmpView: String = "tmpresultdf"

  override def find(df: DataFrame,
                    geometryCol: String = "geometry",
                    timestampCol: String = "ts",
                    featuresCol: String = "features",
                    labelCol: String = "label",
                    weightCol: String = "weight",
                    rowNumberCol: String = "rn",
                    kernelName: String = "kernel",
                    trainView: String = "traindf",
                    unknownView: String = "unknowndf"): DataFrame = {

    val selectUnknownLabel = if (df.columns.contains(labelCol)) {
      (s"$unknownView.$labelCol AS ${unknownView}_$labelCol,", s"${unknownView}_$labelCol,")
    } else {
      ("", "")
    }

    var result = df.sqlContext.sql(
      s"""
         | SELECT ${unknownView}_$rowNumberCol,
         |        ${unknownView}_$geometryCol,
         |        ${unknownView}_$timestampCol,
         |        ${unknownView}_$featuresCol,
         |        ${selectUnknownLabel._2}
         |        ${trainView}_$featuresCol,
         |        ${trainView}_$labelCol,
         |        spatial_distance,
         |        temporal_distance,
         |        spatio_temporal_distance
         | FROM (SELECT ${unknownView}_$rowNumberCol,
         |              ${unknownView}_$geometryCol,
         |              ${unknownView}_$timestampCol,
         |              ${unknownView}_$featuresCol,
         |              ${selectUnknownLabel._2}
         |              ${trainView}_$featuresCol,
         |              ${trainView}_$labelCol,
         |              spatial_distance,
         |              temporal_distance,
         |              $lambda * spatial_distance * spatial_distance + $mi * temporal_distance * temporal_distance AS spatio_temporal_distance,
         |              ROW_NUMBER() OVER(PARTITION BY ${unknownView}_$rowNumberCol ORDER BY ($lambda * spatial_distance * spatial_distance + $mi * temporal_distance * temporal_distance)) AS neighbour_number
         |       FROM (SELECT $unknownView.$rowNumberCol AS ${unknownView}_$rowNumberCol,
         |                    $unknownView.$geometryCol AS ${unknownView}_$geometryCol,
         |                    $unknownView.$timestampCol AS ${unknownView}_$timestampCol,
         |                    $unknownView.$featuresCol AS ${unknownView}_$featuresCol,
         |                    ${selectUnknownLabel._1}
         |                    $trainView.$featuresCol AS ${trainView}_$featuresCol,
         |                    $trainView.$labelCol AS ${trainView}_$labelCol,
         |                    ST_Distance($unknownView.$geometryCol, $trainView.$geometryCol) * 111000.0 AS spatial_distance,
         |                    abs(unix_timestamp($unknownView.$timestampCol) - unix_timestamp($trainView.$timestampCol)) AS temporal_distance
         |            FROM $unknownView CROSS JOIN $trainView)
         |       )
         | WHERE neighbour_number <= $bandwidthNeighbours
         |""".stripMargin
    )

    result.createOrReplaceTempView(tmpView)
    result = df.sqlContext.sql(
      s"""
         | SELECT ${unknownView}_$rowNumberCol,
         |        ${unknownView}_$featuresCol,
         |        ${unknownView}_$geometryCol,
         |        ${unknownView}_$timestampCol,
         |        ${selectUnknownLabel._2}
         |        ${trainView}_$featuresCol,
         |        ${trainView}_$labelCol,
         |        $kernelName(
         |            $tmpView.spatial_distance,
         |            sqrt(max($tmpView.spatio_temporal_distance) OVER(PARTITION BY $tmpView.${unknownView}_$rowNumberCol)) / $lambda,
         |            $tmpView.temporal_distance,
         |            sqrt(max($tmpView.spatio_temporal_distance) OVER(PARTITION BY $tmpView.${unknownView}_$rowNumberCol)) / $mi
         |        ) AS $weightCol
         | FROM $tmpView
         |""".stripMargin
    )

    result
  }
}
