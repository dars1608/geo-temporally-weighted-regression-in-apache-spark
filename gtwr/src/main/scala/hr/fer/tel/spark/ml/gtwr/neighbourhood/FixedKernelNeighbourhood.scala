package hr.fer.tel.spark.ml.gtwr.neighbourhood

import org.apache.spark.sql.DataFrame

import scala.concurrent.duration.{Duration, SECONDS}

/**
 * Kernel neighbourhood which uses predefined spatial and temporal bandwidth which will be passed to kernel functions
 * while evaluating the weights of samples from the train dataset.
 *
 * @param spatialBandwidthMeters spatial bandwidth in meters
 * @param temporalBandwidth temporal bandwidth
 */
class FixedKernelNeighbourhood(val spatialBandwidthMeters: Double = 10.0,
                               val temporalBandwidth: Duration = Duration.create(1, SECONDS)) extends KernelNeighbourhood {

  // using approximation: 1 degree = 111 km = 111 000 m
  private val spatialBandwidthDegrees = spatialBandwidthMeters / 111000.0
  private val temporalBandwidthSeconds = temporalBandwidth.toSeconds

  override def find(df: DataFrame,
                    geometryCol: String = "geometry",
                    timestampCol: String = "ts",
                    featuresCol: String = "features",
                    labelCol: String = "label",
                    weightCol: String = "weight",
                    rowNumberCol: String = "rn",
                    kernelName: String = "kernel",
                    trainView: String = "traindf",
                    unknownView: String = "unknowndf"): DataFrame = {
    val selectUnknownLabel = if (df.columns.contains(labelCol)) {
      (s"$unknownView.$labelCol AS ${unknownView}_$labelCol", s"${unknownView}_$labelCol")
    } else {
      ("", "")
    }

    val result = df.sqlContext.sql(
      s"""
         | SELECT $unknownView.$rowNumberCol AS ${unknownView}_$rowNumberCol,
         |        $unknownView.$featuresCol AS ${unknownView}_$featuresCol,
         |        $unknownView.$geometryCol AS ${unknownView}_$geometryCol,
         |        $unknownView.$timestampCol AS ${unknownView}_$timestampCol,
         |        ${selectUnknownLabel._1},
         |        $trainView.$featuresCol AS ${trainView}_$featuresCol,
         |        $trainView.$labelCol AS ${trainView}_$labelCol,
         |        $kernelName(
         |          ST_Distance($unknownView.$geometryCol, $trainView.$geometryCol),
         |          $spatialBandwidthDegrees,
         |          abs(unix_timestamp($unknownView.$timestampCol) - unix_timestamp($trainView.$timestampCol)),
         |          $temporalBandwidthSeconds
       |          ) as $weightCol
         | FROM $unknownView CROSS JOIN $trainView
         | WHERE ST_Distance($unknownView.$geometryCol, $trainView.$geometryCol) <= $spatialBandwidthDegrees AND
         |        abs(unix_timestamp($unknownView.$timestampCol) - unix_timestamp($trainView.$timestampCol)) <= $temporalBandwidthSeconds
         |""".stripMargin
    )

    result
  }
}
