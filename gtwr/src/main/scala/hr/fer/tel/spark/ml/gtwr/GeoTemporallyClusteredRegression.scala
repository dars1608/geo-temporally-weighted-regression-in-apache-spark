package hr.fer.tel.spark.ml.gtwr

import hr.fer.tel.spark.ml.gtwr.cluster._
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.ml.param.{Param, ParamMap, Params}
import org.apache.spark.ml.regression.{GeneralizedLinearRegression, GeneralizedLinearRegressionModel}
import org.apache.spark.ml.util._
import org.apache.spark.ml.{Estimator, Model}
import org.apache.spark.sql.types.{DoubleType, StructField, StructType}
import org.apache.spark.sql.{Column, DataFrame, Dataset, SparkSession}

import java.io.PrintWriter
import java.util.Objects.nonNull
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}
import scala.io.Source
import scala.language.postfixOps


object GeoTemporallyClusteredRegression {
  private[gtwr] val _clusterCol = "cluster"
  private[gtwr] val _geoTemporalCoords = "gtc"
}

trait GeoTemporallyClusteredRegressionParams extends Params with HasGeometryCol with HasGeneralizedLinearRegression with HasTimestampCol {
  def geoTemporalClustering: Param[GeoTemporalClustering] = new Param[GeoTemporalClustering](
    this, "geoTemporalClustering", "GeoTemporalClustering implementation used for grouping geoTemporal data.")

  def getGeoTemporalClustering: GeoTemporalClustering = $(geoTemporalClustering)
}

class GeoTemporallyClusteredRegression(override val uid: String)(implicit spark: SparkSession)
  extends Estimator[GeoTemporallyClusteredRegressionModel]
    with Serializable with GeoTemporallyClusteredRegressionParams {

  import GeoTemporallyClusteredRegression._

  def this()(implicit spark: SparkSession) = this(Identifiable.randomUID("gtcr"))(spark)

  def setGeometryCol(value: String): GeoTemporallyClusteredRegression = set(geometryCol, value)

  setDefault(geometryCol, "geometry")

  def setTimestampCol(value: String): GeoTemporallyClusteredRegression = set(timestampCol, value)

  setDefault(timestampCol, "ts")

  def setGeoTemporalClustering(value: GeoTemporalClustering): GeoTemporallyClusteredRegression = set(geoTemporalClustering, value)

  setDefault(geoTemporalClustering, new GeoTemporalKMeans(new KMeans(), new GeoTemporalCoordinateSystemTransformer(1, 1)))

  def setGeneralizedLinearRegression(value: GeneralizedLinearRegression): GeoTemporallyClusteredRegression = set(generalizedLinearRegression, value)

  override def fit(dataset: Dataset[_]): GeoTemporallyClusteredRegressionModel = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    var df = dataset.toDF()
    import spark.implicits._

    val clusteringModel = getGeoTemporalClustering.fit(df,
      geoTemporalCoordsCol = _geoTemporalCoords,
      geometryCol = getGeometryCol,
      timestampCol = getTimestampCol,
      clusterCol = _clusterCol
    )

    df = clusteringModel.transform(
      df,
      geometryCol = getGeometryCol,
      timestampCol = getTimestampCol,
      geoTemporalCoordsCol = _geoTemporalCoords
    )

    val glrModels = (0 until clusteringModel.k) map { clusterId =>
      Future {
        val trainDf = df.where($"${_clusterCol}" === clusterId)
        if (trainDf.count() == 0L) {
          null
        } else {
          (clusterId, getGeneralizedLinearRegression.fit(trainDf))
        }
      }(ExecutionContext.global)
    } map {
      job => Await.ready(job, Duration.Inf)
    } map {
      job => Await.result(job, Duration.Inf)
    } filter nonNull toMap

    new GeoTemporallyClusteredRegressionModel(clusteringModel, glrModels)
      .setGeometryCol(getGeometryCol)
      .setTimestampCol(getTimestampCol)
  }

  override def copy(extra: ParamMap): GeoTemporallyClusteredRegression = {
    new GeoTemporallyClusteredRegression(uid)
      .setGeoTemporalClustering(extra.getOrElse(geoTemporalClustering, getGeoTemporalClustering))
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setTimestampCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setGeneralizedLinearRegression(extra.getOrElse(generalizedLinearRegression, getGeneralizedLinearRegression.copy(extra)))
  }

  override def transformSchema(schema: StructType): StructType = schema
}

object GeoTemporallyClusteredRegressionModel extends MLReadable[GeoTemporallyClusteredRegressionModel] {

  private var _clusteringModelLoader: GeoTemporalClusteringModelLoader = GeoTemporalKMeansModel

  def setClusteringModelLoader(clusteringModelLoader: GeoTemporalClusteringModelLoader): GeoTemporallyClusteredRegressionModel.type = {
    _clusteringModelLoader = clusteringModelLoader
    this
  }

  override def read: MLReader[GeoTemporallyClusteredRegressionModel] = {
    new GeoTemporallyClusteredRegressionModelReader(_clusteringModelLoader)
  }

  private[gtwr] class GeoTemporallyClusteredRegressionModelReader(clusteringModelLoader: GeoTemporalClusteringModelLoader)
    extends MLReader[GeoTemporallyClusteredRegressionModel] {

    override def load(path: String): GeoTemporallyClusteredRegressionModel = {
      implicit val spark: SparkSession = this.sparkSession

      try {
        val clusteringModel = clusteringModelLoader.load(s"$path/_clusteringModel")
        val glrModels = (0 until clusteringModel.k)
          .map {
            clusterId =>
              (clusterId, try {
                GeneralizedLinearRegressionModel.load(s"$path/_$clusterId")
              } catch {
                case _: Throwable => null
              })
          } filter { t => t._2 != null } toMap

        val columnsSource = Source.fromFile(s"$path/params")
        val columns = columnsSource.mkString.split(",")
        columnsSource.close()

        new GeoTemporallyClusteredRegressionModel(clusteringModel, glrModels)
          .setGeometryCol(columns(0))
          .setTimestampCol(columns(1))
      } catch {
        case ex: Exception =>
          throw new IllegalArgumentException(
            s"Loading instance of ${GeoTemporallyClusteredRegressionModel.getClass} from $path failed",
            ex
          )
      }
    }
  }

}

class GeoTemporallyClusteredRegressionModel private[gtwr](override val uid: String,
                                                          private val _clusteringModel: GeoTemporalClusteringModel,
                                                          private val _glrModels: Map[Int, GeneralizedLinearRegressionModel])
                                                         (implicit spark: SparkSession)
  extends Model[GeoTemporallyClusteredRegressionModel] with HasGeometryCol with HasTimestampCol
    with MLWritable {

  import GeoTemporallyClusteredRegression._

  private[gtwr] def this(_clusteringModel: GeoTemporalClusteringModel,
                         _glrModels: Map[Int, GeneralizedLinearRegressionModel])(implicit spark: SparkSession) =
    this(Identifiable.randomUID("gtcrModel"), _clusteringModel, _glrModels)(spark)

  def setGeometryCol(value: String): GeoTemporallyClusteredRegressionModel = set(geometryCol, value)

  setDefault(geometryCol, "geometry")

  def setTimestampCol(value: String): GeoTemporallyClusteredRegressionModel = set(timestampCol, value)

  setDefault(timestampCol, "ts")

  override def transform(dataset: Dataset[_]): DataFrame = {
    if (dataset == null) {
      throw new IllegalArgumentException("Input should not be null.")
    }

    val df = dataset.toDF()
    import org.apache.spark.sql.functions.{col, lit}
    import spark.implicits._

    val selectColumns: Seq[Column] = df.columns.toList.map(col) ++ Seq(col(_glrModels.head._2.getPredictionCol))
    val tempDf = _clusteringModel.transform(df,
      geometryCol = getGeometryCol,
      timestampCol = getTimestampCol,
      geoTemporalCoordsCol = _geoTemporalCoords)

    (0 until _clusteringModel.k) map { clusterId =>
      Future {
        val testDf = tempDf.where($"${_clusterCol}" === clusterId)

        if (testDf.count() == 0) {
          testDf.withColumn(_glrModels.head._2.getPredictionCol, lit(null).cast(DoubleType))
        } else {
          _glrModels.get(clusterId) match {
            case None => testDf.withColumn(_glrModels.head._2.getPredictionCol, lit(null).cast(DoubleType))
            case Some(glr) => glr.transform(testDf)
          }
        }
      }(ExecutionContext.global)
    } map {
      job => Await.ready(job, Duration.Inf)
    } map {
      job => Await.result(job, Duration.Inf)
    } reduce {
      (df1, df2) => df1.union(df2)
    } select (selectColumns: _*)
  }

  override def copy(extra: ParamMap): GeoTemporallyClusteredRegressionModel = {
    new GeoTemporallyClusteredRegressionModel(uid, _clusteringModel, _glrModels)
      .setGeometryCol(extra.getOrElse(geometryCol, getGeometryCol))
      .setTimestampCol(extra.getOrElse(timestampCol, getTimestampCol))
  }

  override def transformSchema(schema: StructType): StructType =
    schema.add(StructField(_glrModels.head._2.getPredictionCol, DoubleType))

  override def write: MLWriter = new GeoTemporallyClusteredRegressionModelWriter()

  private[gtwr] class GeoTemporallyClusteredRegressionModelWriter extends MLWriter {
    override def saveImpl(path: String): Unit = {
      _clusteringModel.save(s"$path/_clusteringModel")
      _glrModels.zipWithIndex.foreach { case (glr, i) => glr._2.save(s"$path/_$i") }
      new PrintWriter(s"$path/params") {
        write(s"$getGeometryCol,$getTimestampCol")
        close()
      }
    }
  }

}
