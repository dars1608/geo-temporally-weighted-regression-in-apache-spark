package hr.fer.tel.spark.ml.gtwr.example

import org.apache.spark.ml.feature.StandardScaler
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator

import java.io.FileInputStream
import java.util.{Properties, Random}
import scala.collection.JavaConverters.propertiesAsScalaMapConverter

object HPSBeijingData extends App {

  import Evaluation._

  def createSparkSession(props: Properties): SparkSession = {
    val builder = SparkSession
      .builder()
      .appName(props.getOrDefault("spark.appName", "GeoTemporalCLustering").toString)
      .master(props.getOrDefault("spark.masterUrl", configs("sparkMaster")).toString)
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("spark.sql.crossJoin.enable", true)
      .config("geospark.join.gridtype", "rtree")
      // enable scheduling parallel job execution
      .config("spark.scheduler.mode", "FAIR")

    builder.config("geospark.global.index", value = true)
    builder.config("geospark.global.indextype", "rtree")

    builder.getOrCreate()
  }

  if (args.length != 1) {
    throw new IllegalArgumentException("Program accepts exactly one argument: configuration file path.")
  }

  val props = new Properties()
  props.load(new FileInputStream(args(0)))
  val configs = props.asScala

  println("Received configuration:")
  for ((k, v) <- configs) printf("%s=%s\n", k, v)

  val randomSeed = configs.get("randomSeed") match {
    case Some(value: String) => value.toInt
    case None => new Random().nextInt()
  }

  println(s"randomSeed=$randomSeed\n")
  val random = new Random(randomSeed.toLong)

  implicit val spark: SparkSession = createSparkSession(props)

  import spark.implicits._

  GeoSparkSQLRegistrator.registerAll(spark)

  val df: DataFrame = BeijingDataLoader.load(configs("inputLocation"))
   // .filter(_ => random.nextDouble() < 0.3)

  df.cache()
  println(s"Number of samples: ${df.count()}")

  val lambdas = Seq(1)
  val mis = Seq(1)
  val ks = Seq(60)
  val maxIter = 150
  val convergenceTol = 1e-9
  val testSplit = 0.05
  val regParams = Seq(0.05)

  if (testSplit <= 0.0 || testSplit > 0.3) {
    throw new IllegalArgumentException("Train split must be between 0 and 0.3!")
  }

  val scaler = new StandardScaler()
    .setInputCol("features")
    .setOutputCol("scaledFeatures")
    .setWithStd(true)
    .setWithMean(true)

  implicit val Array(train, test) = scaler
    .fit(df)
    .transform(df)
    .randomSplit(Array(1 - testSplit, testSplit), seed = randomSeed)

  train.cache()
  test.cache()

  val numSamples = test.count().toInt
  println(s"Number of test samples: $numSamples")

  val evaluationDf = (for {lambda <- lambdas; mi <- mis; k <- ks; regParam <- regParams} yield (lambda, mi, k, regParam)) map {
    params =>
      evaluateGTCRKMeans(train, test, params._4, maxIter, convergenceTol, params._3, params._1, params._2) match {
        case (model, fitTime, transformTime, rmse, mse, r2, mae, aic, mape) => (
          model,
          params._1,
          params._2,
          params._3,
          params._4,
          fitTime,
          transformTime,
          rmse,
          mse,
          r2,
          mae,
          aic, mape
        )
      }
  } toDF("model", "lambda", "mi", "k", "reg_param", "fit_time", "transform_time", "rmse", "mse", "r2", "mae", "aic, mape")

  val baselineDf = Seq(0.1) map { (regParam: Double) =>
    evaluateGLR(train, test, 0.035, maxIter, convergenceTol) match {
      case (model, fitTime, transformTime, rmse, mse, r2, mae, aic, mape) => (
        model,
        regParam,
        fitTime,
        transformTime,
        rmse,
        mse,
        r2,
        mae,
        aic, mape
      )
    }
  } toDF("model", "reg_param", "fit_time", "transform_time", "rmse", "mse", "r2", "mae", "aic, mape")

  evaluationDf.show(100, false)
  evaluationDf.write.csv(s"${System.currentTimeMillis()}-${configs("outputLocation")}")

  baselineDf.show(100, false)

  evaluationDf.createTempView("results")
  spark.sql(
    "select * from results having r2 == max(r2)"
  )
}
