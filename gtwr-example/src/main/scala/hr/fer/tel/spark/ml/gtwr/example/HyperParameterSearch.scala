package hr.fer.tel.spark.ml.gtwr.example

import hr.fer.tel.spark.ml.gtwr.GeoTemporallyClusteredRegression
import hr.fer.tel.spark.ml.gtwr.cluster.{GeoTemporalCoordinateSystemTransformer, GeoTemporalKMeans}
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.feature.StandardScaler
import org.apache.spark.ml.regression.GeneralizedLinearRegression
import org.apache.spark.ml.clustering.KMeans
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator
import org.slf4j.LoggerFactory

import java.io.FileInputStream
import java.util.Properties

object HyperParameterSearch extends App {

  val logger = LoggerFactory.getLogger("HyperParameterSearch")

  def gridSearch(train: DataFrame,
                 validate: DataFrame,
                 labelCol: String = "label",
                 featuresCol: String = "features",
                 predictionCol: String = "prediction",
                 maxIter: Int = 25,
                 convergenceTol: Double = 1E-6,
                 regParams: Array[Double] = Array(0.005, 0.01, 0.05, 0.1),
                 numOfClusters: Array[Int] = Array(3, 5, 11)): Map[String, Any] = {
    val family = "gaussian"
    val link = "identity"

    var bestFamily = family
    var bestLink = link
    var bestReg = regParams(0)
    var bestK = numOfClusters(0)
    var bestResult = Double.MaxValue

    val evaluator = new RegressionEvaluator()
      .setLabelCol(labelCol)
      .setPredictionCol(predictionCol)

    for (reg <- regParams) {
      for (k <- numOfClusters) {
        val glr = new GeneralizedLinearRegression()
          .setFeaturesCol(featuresCol)
          .setRegParam(reg)
          .setFamily(family)
          .setLink(link)
          .setMaxIter(maxIter)
          .setTol(convergenceTol)

        val gKMeans = new GeoTemporalKMeans(new KMeans().setK(k), new GeoTemporalCoordinateSystemTransformer())

        try {
          val gcr = new GeoTemporallyClusteredRegression()
            .setGeoTemporalClustering(gKMeans)
            .setGeneralizedLinearRegression(glr)
            .fit(train)

          val predicted = gcr.transform(validate)
          val result = evaluator.evaluate(predicted)
          if (result < bestResult) {
            bestResult = result
            bestFamily = family
            bestLink = link
            bestReg = reg
            bestK = k
          }
        } catch {
          case _: Throwable =>
            logger.warn(
              f"""Evaluation failed for parameters:
                 |Family: $family
                 |Link: $link
                 |Regression: $reg
                 |Clusters: $k""".stripMargin)
        }
      }
    }

    println(
      f"""
         |Family: $bestFamily
         |Link: $bestLink
         |Regression: $bestReg
         |Clusters: $bestK
         |Score: $bestResult""".stripMargin)

    Map(
      "family" -> bestFamily,
      "link" -> bestLink,
      "reg" -> bestReg,
      "k" -> bestK
    )
  }

  def createSparkSession(props: Properties): SparkSession = {
    val builder = SparkSession
      .builder()
      .appName(props.getOrDefault("spark.appName", "GWR").toString)
      .master(props.getOrDefault("spark.masterUrl", "local[*]").toString)
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("geospark.join.gridtype", "rtree")

    builder.config("geospark.global.index", value = true)
    builder.config("geospark.global.indextype", "rtree")

    val numOfPartitions = props.get("geospark.join.numpartition")
    if (numOfPartitions != null) {
      builder.config("geospark.join.numpartition", numOfPartitions.asInstanceOf[Int])
    }

    builder.getOrCreate()
  }

  if (args.length != 1) {
    throw new IllegalArgumentException("Program accepts exactly one argument: configuration file path.")
  }

  val props = new Properties()
  props.load(new FileInputStream(args(0)))

  implicit val spark: SparkSession = createSparkSession(props)
  GeoSparkSQLRegistrator.registerAll(spark)

  val df: DataFrame = DataLoader.load(props.getProperty("inputLocation"), 50)

  val scaler = new StandardScaler()
    .setInputCol("features")
    .setOutputCol("scaledFeatures")
    .setWithStd(true)
    .setWithMean(true)

  val Array(training, test) = scaler
    .fit(df)
    .transform(df)
    .randomSplit(Array(0.9, 0.1), seed = 16081997)

  val regParams = props
    .getProperty("hyperParameter.regressionParams")
    .split(",") map { x => x.toDouble }

  val numOfClusters = props
    .getProperty("hyperParameter.numOfClusters")
    .split(",") map { x => x.toInt }

  val maxIter = props
    .getProperty("hyperParameter.maxIter")
    .toInt

  val convergenceTol = props
    .getProperty("hyperParameter.convergenceTol")
    .toDouble

  gridSearch(training, test,
    featuresCol = "scaledFeatures",
    regParams = regParams,
    numOfClusters = numOfClusters,
    maxIter = maxIter,
    convergenceTol = convergenceTol
  )
}
