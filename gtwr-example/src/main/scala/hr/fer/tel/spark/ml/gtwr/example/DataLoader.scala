package hr.fer.tel.spark.ml.gtwr.example

import org.apache.spark.ml.feature.VectorAssembler
import org.apache.spark.sql.types.{DoubleType, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}

object DataLoader {

  def load(inputLocation: String, numOfFeatures: Int)(implicit spark: SparkSession): DataFrame = {
    val (inputSchema, featureColumns) = generateSchemaDescriptor(numOfFeatures)
    val df = spark
      .read
      .format("csv")
      .option("header", "true")
      .schema(inputSchema)
      .load(inputLocation)

    new VectorAssembler()
      .setInputCols(featureColumns)
      .setOutputCol("features")
      .transform(df.na.drop)
      .selectExpr(
        "ST_Point(CAST(lon AS Decimal(24,20)), CAST(lat AS Decimal(24,20))) AS geometry",
        "CAST(ts AS Timestamp) as ts",
        "y AS label",
        "features"
      )
  }

  private def generateSchemaDescriptor(numOfFeatures: Int) = {
    val featureColumns: Array[String] = (0 until numOfFeatures).map(i => f"x$i").toArray

    var inputSchema: StructType = new StructType()
      .add("lat", DoubleType, nullable = false)
      .add("lon", DoubleType, nullable = false)
      .add("ts", DoubleType, nullable = false)
    for (i <- 0 until numOfFeatures) {
      inputSchema = inputSchema.add(f"x$i", DoubleType, nullable = false)
    }
    inputSchema = inputSchema.add("y", DoubleType, nullable = false)
    (inputSchema, featureColumns)
  }
}
