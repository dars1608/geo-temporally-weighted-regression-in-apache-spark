package hr.fer.tel.spark.ml.gtwr.example

import hr.fer.tel.spark.ml.gtwr.cluster.{GeoTemporalBisectingKMeans, GeoTemporalCoordinateSystemTransformer, GeoTemporalGaussianMixture, GeoTemporalKMeans}
import hr.fer.tel.spark.ml.gtwr.kernel.Gaussian2DKernel
import hr.fer.tel.spark.ml.gtwr.neighbourhood.{AdaptiveKernelNeighbourhood, FixedKernelNeighbourhood, KernelNeighbourhood}
import hr.fer.tel.spark.ml.gtwr.{GeoTemporallyClusteredRegression, GeoTemporallyWeightedRegression}
import org.apache.spark.ml.clustering.{BisectingKMeans, GaussianMixture, KMeans}
import org.apache.spark.ml.evaluation.RegressionEvaluator
import org.apache.spark.ml.linalg.DenseVector
import org.apache.spark.ml.regression.GeneralizedLinearRegression
import org.apache.spark.sql.{DataFrame, SparkSession}

object Evaluation {

  private val rmseEvaluator = new RegressionEvaluator().setMetricName("rmse")
  private val mseEvaluator = new RegressionEvaluator().setMetricName("mse")
  private val r2Evaluator = new RegressionEvaluator().setMetricName("r2")
  private val maeEvaluator = new RegressionEvaluator().setMetricName("mae")

  def time[R](block: => R): (R, Double) = {
    val t0 = System.nanoTime()
    val result = block
    val t1 = System.nanoTime()

    (result, (t1 - t0) * 1d / 1000000000)
  }

  def calculateAIC(df: DataFrame): Double = {
    val N = df.count()
    val k = df.first().getAs[DenseVector]("features").size + 1
    val sserr = df.selectExpr("(label - prediction) * (label - prediction) as s").agg("s" -> "sum").first().getDouble(0)

    N * (math.log(sserr / N * 2.0 * math.Pi) + 1.0) + 2.0 + 2*k
  }

  def calculateMAPE(df: DataFrame): Double =
    df.selectExpr("abs((label - prediction) / label) as s")
      .agg("s" -> "avg")
      .first()
      .getDouble(0) * 100

  def evaluateModel(transformFunction: DataFrame => DataFrame, test: DataFrame): (Double, Double, Double, Double, Double, Double, Double) = {
    val (predictedDf, transformTime) = time {
      transformFunction(test)
    }

    val rmse = rmseEvaluator.evaluate(predictedDf)
    val mse = mseEvaluator.evaluate(predictedDf)
    val r2 = r2Evaluator.evaluate(predictedDf)
    val mae = maeEvaluator.evaluate(predictedDf)
    val aic = calculateAIC(predictedDf)
    val mape = calculateMAPE(predictedDf)

    (transformTime, rmse, mse, r2, mae, aic, mape)
  }

  def evaluateGLR(train: DataFrame, test: DataFrame, regParam: Double, maxIter: Int, convergenceTol: Double): (String, Double, Double, Double, Double, Double, Double, Double, Double) = {
    val glr = createGLR(regParam, maxIter, convergenceTol)

    val (glrModel, fitTime) = time {
      glr.fit(train)
    }

    val (transformTime, rmse, mse, r2, mae, aic, mape) = evaluateModel(glrModel.transform, test)
    ("Generalised Linear Regression", fitTime, transformTime, rmse, mse, r2, mae, aic, mape)
  }

  def evaluateGTWR(neighbourhood: KernelNeighbourhood, train: DataFrame, test: DataFrame, regParam: Double, maxIter: Int, convergenceTol: Double, numSamples: Int)(implicit spark: SparkSession): (Double, Double, Double, Double, Double, Double, Double, Double) = {
    val glr = new GeneralizedLinearRegression()
      .setFeaturesCol("scaledFeatures")
      .setWeightCol("weight")
      .setRegParam(regParam)
      .setFamily("gaussian")
      .setLink("identity")
      .setMaxIter(maxIter)
      .setTol(convergenceTol)

    val (gtwrModel, fitTime) = time {
      new GeoTemporallyWeightedRegression()
        .setKernelNeighbourhood(neighbourhood)
        .setGeneralizedLinearRegression(glr)
        .setKernel(new Gaussian2DKernel())
        .setMaxInputSize(numSamples)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae, aic, mape) = evaluateModel(gtwrModel.transform, test)
    (fitTime, transformTime, rmse, mse, r2, mae, aic, mape)
  }

  def evaluateGTWRFixedKernel(train: DataFrame, test: DataFrame, regParam: Double, maxIter: Int, convergenceTol: Double, numSamples: Int, bandwidthKm: Double)(implicit spark: SparkSession): (String, Double, Double, Double, Double, Double, Double, Double, Double) = {
    val (fitTime, transformTime, rmse, mse, r2, mae, aic, mape) = evaluateGTWR(new FixedKernelNeighbourhood(spatialBandwidthMeters = bandwidthKm), train, test, regParam, maxIter, convergenceTol, numSamples)
    ("Geo-Temporally Weighted Regression (Fixed Kernel)", fitTime, transformTime, rmse, mse, r2, mae, aic, mape)
  }

  def evaluateGTWRVariableKernel(train: DataFrame, test: DataFrame, regParam: Double, maxIter: Int, convergenceTol: Double, numSamples: Int, numOfNeighbours: Int)(implicit spark: SparkSession): (String, Double, Double, Double, Double, Double, Double, Double, Double) = {
    val (fitTime, transformTime, rmse, mse, r2, mae, aic, mape) = evaluateGTWR(new AdaptiveKernelNeighbourhood(numOfNeighbours), train, test, regParam, maxIter, convergenceTol, numSamples)
    ("Geo-Temporally Weighted Regression (Variable Kernel)",fitTime, transformTime, rmse, mse, r2, mae, aic, mape)
  }

  def evaluateGTCRKMeans(train: DataFrame, test: DataFrame, regParam: Double, maxIter: Int, convergenceTol: Double, numOfClusters: Int, lambda: Double, mi: Double)(implicit spark: SparkSession): (String, Double, Double, Double, Double, Double, Double, Double, Double) = {
    val glr = createGLR(regParam, maxIter, convergenceTol)

    val (gtcrModel, fitTime) = time {
      new GeoTemporallyClusteredRegression()
        .setGeoTemporalClustering(new GeoTemporalKMeans(new KMeans().setK(numOfClusters), new GeoTemporalCoordinateSystemTransformer(lambda, mi)))
        .setGeneralizedLinearRegression(glr)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae, aic, mape) = evaluateModel(gtcrModel.transform, test)
    ("Geo-Temporally Clustered Regression (K-Means)", fitTime, transformTime, rmse, mse, r2, mae, aic, mape)
  }

  def evaluateGTCRBisectingKMeans(train: DataFrame, test: DataFrame, regParam: Double, maxIter: Int, convergenceTol: Double, numOfClusters: Int, lambda: Double, mi: Double)(implicit spark: SparkSession): (String, Double, Double, Double, Double, Double, Double, Double, Double) = {
    val glr = createGLR(regParam, maxIter, convergenceTol)

    val (gtcrModel, fitTime) = time {
      new GeoTemporallyClusteredRegression()
        .setGeoTemporalClustering(new GeoTemporalBisectingKMeans(new BisectingKMeans().setK(numOfClusters), new GeoTemporalCoordinateSystemTransformer(lambda, mi)))
        .setGeneralizedLinearRegression(glr)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae, aic, mape) = evaluateModel(gtcrModel.transform, test)
    ("Geo-Temporally Clustered Regression (Bisecting K-Means)", fitTime, transformTime, rmse, mse, r2, mae, aic, mape)
  }

  def evaluateGTCRGaussianMixtures(train: DataFrame, test: DataFrame, regParam: Double, maxIter: Int, convergenceTol: Double, numOfClusters: Int, lambda: Double, mi: Double)(implicit spark: SparkSession): (String, Double, Double, Double, Double, Double, Double, Double, Double) = {
    val glr = createGLR(regParam, maxIter, convergenceTol)

    val (gtcrModel, fitTime) = time {
      new GeoTemporallyClusteredRegression()
        .setGeoTemporalClustering(new GeoTemporalGaussianMixture(new GaussianMixture().setK(numOfClusters), new GeoTemporalCoordinateSystemTransformer(lambda, mi)))
        .setGeneralizedLinearRegression(glr)
        .fit(train)
    }

    val (transformTime, rmse, mse, r2, mae, aic, mape) = evaluateModel(gtcrModel.transform, test)
    ("Geo-Temporally Clustered Regression (Gaussian Mixture)", fitTime, transformTime, rmse, mse, r2, mae, aic, mape)
  }

  private def createGLR(regParam: Double, maxIter: Int, convergenceTol: Double) = {
    new GeneralizedLinearRegression()
      .setFeaturesCol("scaledFeatures")
      .setRegParam(regParam)
      .setFamily("gaussian")
      .setLink("identity")
      .setMaxIter(maxIter)
      .setTol(convergenceTol)
  }
}
