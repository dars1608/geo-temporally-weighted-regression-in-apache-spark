package hr.fer.tel.spark.ml.gtwr.example

import org.apache.spark.ml.feature.{StringIndexer, VectorAssembler}
import org.apache.spark.sql.types.{DoubleType, StringType, StructType}
import org.apache.spark.sql.{DataFrame, SparkSession}

object BeijingDataLoader {

  private val inputSchema = new StructType()
    .add("PM2_5", DoubleType, nullable = true)
    .add("PM10", DoubleType, nullable = true)
    .add("SO2", DoubleType, nullable = true)
    .add("NO2", DoubleType, nullable = true)
    .add("CO", DoubleType, nullable = true)
    .add("O3", DoubleType, nullable = true)
    .add("TEMP", DoubleType, nullable = false)
    .add("PRES", DoubleType, nullable = false)
    .add("DEWP", DoubleType, nullable = false)
    .add("RAIN", DoubleType, nullable = false)
    .add("wd", StringType, nullable = true)
    .add("WSPM", DoubleType, nullable = true)
    .add("latitude", DoubleType, nullable = false)
    .add("longitude", DoubleType, nullable = false)
    .add("timestamp", DoubleType, nullable = false)

  private val featureColumns = Seq("PM10", "SO2", "NO2", "CO", "O3", "TEMP", "PRES", "DEWP", "RAIN", "WSPM", "WDI").toArray

  def load(inputLocation: String)(implicit spark: SparkSession): DataFrame = {
    var df = spark
      .read
      .format("csv")
      .option("header", "true")
      .schema(inputSchema)
      .load(inputLocation)

    df = new StringIndexer()
      .setInputCol("wd")
      .setOutputCol("WDI")
      .setHandleInvalid("skip")
      .fit(df)
      .transform(df)

    new VectorAssembler()
      .setInputCols(featureColumns)
      .setOutputCol("features")
      .transform(df.na.drop)
      .selectExpr(
        "ST_Point(CAST(longitude AS Decimal(24,20)), CAST(latitude AS Decimal(24,20))) AS geometry",
        "CAST(timestamp AS Timestamp) as ts",
        "PM2_5 AS label",
        "features"
      )
  }
}
