package hr.fer.tel.spark.ml.gtwr.example

import org.apache.spark.ml.feature.StandardScaler
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.sql.{DataFrame, SparkSession}
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geosparksql.utils.GeoSparkSQLRegistrator

import java.io.FileInputStream
import java.util.{Properties, Random}
import scala.collection.JavaConverters.propertiesAsScalaMapConverter

object BeijingMain extends App {

  import Evaluation._

  def createSparkSession(props: Properties): SparkSession = {
    val builder = SparkSession
      .builder()
      .appName(props.getOrDefault("spark.appName", "GTWR").toString)
      .master(props.getOrDefault("spark.masterUrl", configs("sparkMaster")).toString)
      .config("spark.serializer", classOf[KryoSerializer].getName)
      .config("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .config("spark.sql.crossJoin.enable", true)
      .config("geospark.join.gridtype", "rtree")
      // enable scheduling parallel job execution
      .config("spark.scheduler.mode", "FAIR")

    builder.config("geospark.global.index", value = true)
    builder.config("geospark.global.indextype", "rtree")

    builder.getOrCreate()
  }

  if (args.length != 1) {
    throw new IllegalArgumentException("Program accepts exactly one argument: configuration file path.")
  }

  val props = new Properties()
  props.load(new FileInputStream(args(0)))
  val configs = props.asScala

  println("Received configuration:")
  for ((k, v) <- configs) printf("%s=%s\n", k, v)

  implicit val spark: SparkSession = createSparkSession(props)

  import spark.implicits._

  GeoSparkSQLRegistrator.registerAll(spark)

  val df: DataFrame = BeijingDataLoader.load(configs("inputLocation"))
  df.persist()
  println(s"Number of samples: ${df.count()}")


  val regParam = configs("glr.regressionParam").toDouble
  val maxIter = configs("glr.maxIter").toInt
  val convergenceTol = configs("glr.convergenceTol").toDouble
  val numOfClusters = configs("gtcr.numOfClusters").toInt
  val numOfNeighbours = configs("gtwr.numOfNeighbours").toInt
  val bandwidthKm = configs("gtwr.bandwidthKm").toDouble
  val testSplit = configs("testSplit").toDouble
  val lambda = configs("gtcr.lambda").toDouble
  val mi = configs("gtcr.mi").toDouble

  val randomSeed = configs.get("randomSeed") match {
    case Some(value: String) => value.toInt
    case None => new Random().nextInt()
  }

  println(s"randomSeed=$randomSeed\n")

  if (testSplit <= 0.0 || testSplit > 0.3) {
    throw new IllegalArgumentException("Train split must be between 0 and 0.3!")
  }

  val scaler = new StandardScaler()
    .setInputCol("features")
    .setOutputCol("scaledFeatures")
    .setWithStd(true)
    .setWithMean(true)

  implicit val Array(train, test) = scaler
    .fit(df)
    .transform(df)
    .randomSplit(Array(1 - testSplit, testSplit), seed = randomSeed)

  train.persist()
  test.persist()
  df.unpersist()

  val numSamples = test.count().toInt
  println(s"Number of test samples: $numSamples")

  val evaluationDf = Seq(
    evaluateGLR(train, test, regParam, maxIter, convergenceTol)
    , evaluateGTWRFixedKernel(train, test, regParam, maxIter, convergenceTol, numSamples, bandwidthKm)
    , evaluateGTWRVariableKernel(train, test, regParam, maxIter, convergenceTol, numSamples, numOfNeighbours)
    , evaluateGTCRKMeans(train, test, regParam, maxIter, convergenceTol, numOfClusters, lambda, mi)
    , evaluateGTCRBisectingKMeans(train, test, regParam, maxIter, convergenceTol, numOfClusters, lambda, mi)
    , evaluateGTCRGaussianMixtures(train, test, regParam, maxIter, convergenceTol, numOfClusters, lambda, mi)
  ).toDF("modelName", "fitTimeSeconds", "transformTimeSeconds",
    "rootMeanSquareError", "meanSquareError", "r^2", "meanAbsoluteError")

  evaluationDf.show(false)
  // evaluationDf.write.csv(s"${System.currentTimeMillis()}-${configs("outputLocation")}")
  // println(s"Train data size = ${train.count()}; Test data size = ${test.count()}")
}
